from bson import ObjectId
from django import forms
from django.shortcuts import render, redirect
# from django.urls import reverse
from django.views.generic import CreateView, ListView, DetailView, FormView
from django.views.generic.base import ContextMixin, TemplateView, View
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import User, Note, Culture
from .serializers import UserSerializer
from django import forms
from django.core import serializers

from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
from djongo import models
import re


# class ListExcursion(ListView):
class ListExcursion(TemplateView):
    # model = Note
    # paginate_by = 4
    template_name = "page_excursions.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class PageExcursion(TemplateView):
    model = Note
    paginate_by = 4
    template_name = "info_excursion.html"
    pk_url_kwarg = "_id"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        this_obj = Note.objects.get(pk=ObjectId(kwargs['id_exc']))
        exc_places = []

        for i in this_obj.places:
            exc_places.append(i['name'])
        all_cultures = Culture.objects.all()
        exc_cultures = [None] * len(exc_places)
        print(exc_places)
        for cul in all_cultures:
            if str(cul.id) in exc_places:
                index_cul = exc_places.index(str(cul.id))
                exc_cultures[index_cul] = cul
        context['cul_objects'] = serializers.serialize("json", exc_cultures)
        context['object'] = this_obj
        # context['object'] = Note.objects.get(pk=ObjectId("601a76a7a31c0f3bd6707344"))
        return context


class PageTestOpenStreetMap(TemplateView):

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class PageTestAzureMap(TemplateView):
    template_name = "test_azure_map.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class CultureForm(forms.ModelForm):
    class Meta:
        model = Culture
        fields = ['name', 'description', 'latitude', 'longitude']

    name = forms.CharField(max_length=100)
    # description = forms.CharField(widget=forms.Textarea)
    description = forms.CharField(max_length=100)
    latitude = models.FloatField()
    longitude = models.FloatField()


class PageCreateCulture(FormView):
    template_name = "create_culture.html"
    form_class = CultureForm
    form = None
    success_url = '/'

    def get(self, request, *args, **kwargs):
        self.form = CultureForm()
        return super(PageCreateCulture, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(PageCreateCulture, self).get_context_data(**kwargs)
        context["form"] = self.form
        return context

    def post(self, request, *args, **kwargs):
        self.form = CultureForm(request.POST, request.FILES)
        if self.form.is_valid():
            # не работает сохранение формы!!!
            # self.form.save()
            Culture.objects.create(name=request.POST['name'],
                                   description=request.POST['description'],
                                   latitude=request.POST['latitude'],
                                   longitude=request.POST['longitude'])
            # return redirect("/page/excursions")
            form = None
            return super(PageCreateCulture, self).get(request, *args, **kwargs)
        else:
            return super(PageCreateCulture, self).get(request, *args, **kwargs)


class ExcursionForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = ['name', 'text', 'price', 'place']

    name = forms.CharField(max_length=100)
    # description = forms.CharField(widget=forms.Textarea)
    text = forms.TextInput()
    price = forms.CharField(max_length=100)
    place = forms.CharField(max_length=100)


class PageCreateExcursion(FormView):
    template_name = "create_excursion.html"
    form_class = ExcursionForm
    form = None

    def get(self, request, *args, **kwargs):
        self.form = ExcursionForm()
        return super(PageCreateExcursion, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['cul_objects'] = serializers.serialize("json", Culture.objects.all())
        return context

    def post(self, request, *args, **kwargs):
        self.form = ExcursionForm(request.POST, request.FILES)
        # print(request.POST)

        if self.form.is_valid():
            # не работает сохранение формы!!!
            # self.form.save()
            site_place = request.POST.getlist('place')
            print(site_place)
            places = []
            for i in site_place:
                places.append({'name': i})
            Note.objects.create(name=request.POST['name'],
                                text=request.POST['text'],
                                price=request.POST['price'],
                                places=places,
                                name_site="source"
                                )
            # return redirect("/page/excursions")
            return super(PageCreateExcursion, self).get(request, *args, **kwargs)
        else:
            return super(PageCreateExcursion, self).get(request, *args, **kwargs)
