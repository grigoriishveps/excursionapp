from django import forms
from django.core.paginator import Paginator
from django.shortcuts import render, redirect
# from django.urls import reverse
from django.views.generic import CreateView, ListView, DetailView
from django.views.generic.base import ContextMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import  User, Note, Culture
from .serializers import *
from django.core import serializers
from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
from djongo import models
import re
# Create your views here.
#
class NoteAPI(APIView):
    def get(self, request):
        print( request.GET)
        objects_note = ''
        try:
            page = request.GET['page']
        except KeyError:
            page = 1
        try:
            page_size = request.GET['page_size']
        except KeyError:
            page_size = 8
        try:
            source_exc = request.GET['source_exc']
        except KeyError:
            source_exc = '0'
        if source_exc == '0':
            source_exc = '.*'
            objects_note = Note.objects.all()
        elif source_exc == '1':
            source_exc = 'source'
            objects_note = Note.objects.filter(name_site='source')
        else:
            source_exc = '~(source)'
            objects_note = Note.objects.exclude(name_site="source")

        articles = Paginator(objects_note , page_size)

        page_art = articles.page(page)
        return Response({"notes": NoteSerializer(articles.page(page).object_list, many=True).data, "page": page, "num_pages": articles.num_pages})


class FunctionSaveDataFromSputnik(APIView):
    def get(self, request ):

        html = urlopen("https://www.sputnik8.com/ru/st-petersburg")
        bsObj = BeautifulSoup(html.read())
        # link = '23710-obzornaya-avtobusnaya-ekskursiya-po-gorodu-s-posescheniem-ermitazha'
        # #print(bsObj.body.div)

        links_set = set()
        bsObj = bsObj.body.div
        #print(bsObj.findAll("a", href=re.compile("^.*(/activities/).+")))
        for link in bsObj.findAll("a", href=re.compile("(/activities/).+")):
            if 'href' in link.attrs:
                if link.attrs['href'] not in links_set:
                    new_page = link.attrs['href']
                    print("https://www.sputnik8.com"+new_page)
                    links_set.add(new_page)
                    content_page = BeautifulSoup(urlopen("https://www.sputnik8.com"+new_page).read())
                    content1 = content_page.find("div", {'id': 'a-description'})
                    content2 = content_page.find("div", {'id': 'a-places'})
                    text = content1.getText()+content2.getText()
                    print(text)
                    new_elem = Note.objects.create(name=new_page, text=text, name_site='sputnik8')

        text = ''
        return render(request, "test_.html", {'cats':text})

class FunctionSaveDataFromTripster(APIView):
    def get(self, request):
        html = urlopen("https://experience.tripster.ru/experience/Saint_Petersburg/")
        bsObj = BeautifulSoup(html.read())
        links_set = set()
        bsObj = bsObj.body.div
        # print(bsObj.findAll("a", href=re.compile("^.*(/activities/).+")))
        i = 0
        for link in bsObj.findAll("a", href=re.compile("(/experience/\d+/).*")):
            if 'href' in link.attrs:
                if int(re.search(r'(?<=/experience/)\d+', link.attrs['href'])[0]) not in links_set:
                    new_page = link.attrs['href']
                    print("https://experience.tripster.ru" + new_page)
                    links_set.add(int(re.search(r'(?<=/experience/)\d+', new_page)[0]))
                    content_page = BeautifulSoup(urlopen("https://experience.tripster.ru" + new_page).read())
                    content1 = content_page.find("div", {'class': 'expage-lead'})
                    content2 = content_page.find("section", {'class': 'expage-content'}).div
                    text = content1.getText() + content2.getText()
                    #print(text)
                    right_content = content_page.find("aside", {'class': 'expage-col-right'}).find_next('div',
                            string=re.compile('Как проходит')).find_next_sibling('div')
                    new_elem = Note.objects.create(name=new_page,
                                                   text=text,
                                                   name_site='tripster',
                                                   full_url=("https://experience.tripster.ru" + new_page),
                                                   is_on_foot=(re.search(r'.*(Пешком).*', right_content.getText()))!=None)
                    if i == 6:
                        break
                    i += 1

        text = ''
        return render(request, "test_.html", {'cats': text})

class FunctionSaveDataFromExcava(APIView):
    def get(self, request):
        html = urlopen("http://excava.ru/")
        bsObj = BeautifulSoup(html.read()).body.find("div", {'id': 'home-portfolio'})
        links_set = set()
        text = ''
        i = 0
        for link in bsObj.findAll("a", {'class': 'portfolio-entry-img-link'}, href=re.compile("(/portfolio/[\w-]+/).*")):
            if 'href' in link.attrs:
                if re.search(r'(?<=/portfolio/)[\w-]+', link.attrs['href'])[0] not in links_set:
                    new_page = link.attrs['href']
                    print(new_page)
                    links_set.add(re.search(r'(?<=/portfolio/)[\w-]+', new_page)[0])
                    content_page = BeautifulSoup(urlopen(new_page).read()).find("article").contents

                    for i in range(0, len(content_page)-2):
                        text += content_page[i].getText()
                    new_elem = Note.objects.create(name=new_page,
                                                   text=text,
                                                   name_site='excava',
                                                   full_url= new_page,
                                                   is_on_foot=True)
                    # if i == 6:
                    #     break
                    # i += 1
        return render(request, "test_.html", {'cats': text})



class FunctionSaveDataFromTest(APIView):
    def get(self, request):
        text=""


        object = Note.objects.get(pk=ObjectId(kwargs['id_exc']))


        return render(request, "test_.html", {'cats': text})


class FunctionSaveDataFromKgiop(APIView):
    def get(self, request):
        html = urlopen("https://kgiop.gov.spb.ru/uchet/list_objects/?page=3")
        bsObj = BeautifulSoup(html.read()).body.find("div", {'class': 'search-result'})
        links_set = set()
        text = ''
        i = 0
        for link in bsObj.findAll("a", href=re.compile("(/uchet/list_objects/\d+/).*")):
            if 'href' in link.attrs:
                if int(re.search(r'(?<=/list_objects/)\d+', link.attrs['href'])[0]) not in links_set:
                    new_page = link.attrs['href']
                    print(new_page)
                    links_set.add(int(re.search(r'(?<=/list_objects/)\d+', link.attrs['href'])[0]))
                    content_page = BeautifulSoup(urlopen("https://kgiop.gov.spb.ru" + new_page).read()).find("div", {'class': 'layerobject_detail__content__data'}).contents

                    # for i in range(0, len(content_page)-2):
                    #     text += content_page[i].getText()
                    new_elem = Culture.objects.create(name=content_page[5].getText().strip(),
                                                      address=content_page[11].getText().strip(),
                                                      source_url="https://kgiop.gov.spb.ru" + new_page,
                                                      type=content_page[20].getText().strip()
                                                   )
                    # if i == 6:
                    #     break
                    # i += 1
        return render(request, "test_.html", {'cats': text})

class FunctionSaveDataFromTonkosti(APIView):
    def get(self, request):
        req = Request("https://tonkosti.ru/%D0%AD%D0%BA%D1%81%D0%BA%D1%83%D1%80%D1%81%D0%B8%D0%B8_%D0%BF%D0%BE_%D0%A1%D0%B0%D0%BD%D0%BA%D1%82-%D0%9F%D0%B5%D1%82%D0%B5%D1%80%D0%B1%D1%83%D1%80%D0%B3%D1%83")
        req.add_header("user-agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36")
        html = urlopen(req)
        bsObj = BeautifulSoup(html.read()).body.find("ul", {'class': 'guide-excursions-list guide-excursions-list--region js-guide-excursions-list'})
        links_set = set()
        text = ''
        i = 0
        for link in bsObj.findAll("li"):
            if 'href' in link.a.attrs:
                if re.search(r'(?<=/excursii/)\w+', link.a.attrs['href'])[0] not in links_set:
                    new_page = link.a.attrs['href']
                    links_set.add(re.search(r'(?<=/excursii/)\w+', new_page)[0])
                    req = Request("https://tonkosti.ru/" + new_page)
                    req.add_header("user-agent",
                                   "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150 Safari/537.36")
                    content_page = BeautifulSoup(urlopen(req).read())
                    main_content = content_page.find("div", {'class': 'excursion__texts'})
                    side_content = content_page.find("div", {'class': 'excursion__main-column-2'})
                    list_content = main_content.find("div", {'class': 'excursion__interest-places-list'})
                    main_content = main_content.children
                    type_text = side_content.find("ul", {'class': 'excursion__info-list'}).find("li").contents[2]
                    places = []
                    if list_content != None:
                        list_content = list_content.contents
                        for x in list_content:
                            places.append(x.getText())

                    for x in main_content:
                        if x.name == 'div':
                            if x['class'] == 'excursion__part-of':
                                break
                            else:
                                continue
                        if x.name == 'div' or x.name == 'p':
                            text += x.getText()

                    new_elem = Note.objects.create(name=new_page,
                                                   text=text,
                                                   name_site='tonkosti',
                                                   full_url= new_page,
                                                   is_on_foot=type_text=="Пешеходная",
                                                   places=','.join(places))
                    if i == 5:
                        break
                    i += 1
        return render(request, "test_.html", {'cats': text})

class TestMap(APIView):
    def get(self, request):
        return render(request, "test_map.html", {})