from django.conf.urls.static import static
from django.urls import path

app_name = "page"

from .views import *
from .page_views import *

urlpatterns = [
    path('data/sputnik', FunctionSaveDataFromSputnik.as_view()),
    path('data/tripster', FunctionSaveDataFromTripster.as_view()),
    path('data/excava', FunctionSaveDataFromExcava.as_view()),
    path('data/kgiop', FunctionSaveDataFromKgiop.as_view()),
    path('data/tonkosti', FunctionSaveDataFromTonkosti.as_view()),
    path('data/test', FunctionSaveDataFromTest.as_view()),

    path('map/test', TestMap.as_view() , name="main"),
    path('map/test_azure', PageTestAzureMap.as_view()),
    path('note', NoteAPI.as_view())
]
