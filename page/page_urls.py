from django.conf.urls.static import static
from django.urls import path

app_name = "page_urls"

from .page_views import *

urlpatterns = [

    path('excursions', ListExcursion.as_view()),
    path('excursions/add', PageCreateExcursion.as_view()),
    path('excursion/<str:id_exc>/', PageExcursion.as_view(), name="page_excursion"),
    path('test_map', PageTestOpenStreetMap.as_view(), name="test_map"),
    path('culture/add', PageCreateCulture.as_view(), name="create_culture")
]
