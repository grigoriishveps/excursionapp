from djongo import models
# Create your models here.

class Culture_Object(models.Model):
    name = models.CharField(max_length=150)

    class Meta:
        abstract = True
    def __str__(self):
        return self.name


class TypeExc(models.Model):
    name = models.CharField(max_length=150)
    val = models.BooleanField(default=False)
    class Meta:
        abstract = True
    def __str__(self):
        return self.name

class User(models.Model):
    class Meta:
        verbose_name = "Пользватель_хаха"
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, blank=True)
    # date_birth = models.DateField( blank=True)
    # phone_number = models.CharField(max_length=16, blank=True)
    email = models.CharField(max_length=30, blank=True)
    sex = models.CharField(max_length=3, blank=True)
    #description = models.TextField()
    def __str__(self):
        return  self.first_name + "_" +self.last_name

class Note(models.Model):
    id = models.ObjectIdField(db_column='_id')
    name = models.CharField(max_length=100)
    text = models.TextField()
    name_site = models.CharField(max_length=100)
    is_on_foot = models.BooleanField(default=False)
    link = models.TextField()
    places = models.ArrayField(model_container=Culture_Object,default=[])
    price = models.TextField()
    max_group_size = models.IntegerField()
    rate = models.FloatField()
    type_arch = models.ArrayField(model_container=TypeExc)

class Culture(models.Model):
    id = models.ObjectIdField(db_column='_id')
    name = models.CharField(max_length=150)
    type = models.CharField(max_length=30)
    source_url = models.CharField(max_length=150)
    address = models.CharField(max_length=150)
    description = models.CharField(max_length=150)
    latitude = models.FloatField()
    longitude = models.FloatField()

