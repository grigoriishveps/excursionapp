from rest_framework import serializers

class UserSerializer(serializers.Serializer):
    first_name = serializers.CharField(max_length=50)
    last_name = serializers.CharField(max_length=50)
    # date_birth = serializers.DateField()
    # phone_number = serializers.CharField(max_length=16)
    email = serializers.CharField(max_length=30)
    sex = serializers.CharField(max_length=3)

class CultureSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    text = serializers.CharField(max_length=100)
    place = serializers.CharField(max_length=100)

class NoteSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=40)
    name = serializers.CharField(max_length=100)
    text = serializers.CharField(max_length=100)
    link = serializers.CharField(max_length=100)
    name_site = serializers.CharField(max_length=100)
    is_on_foot = serializers.BooleanField(default=False)