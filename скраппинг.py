import logging
import os
import time

from bs4 import BeautifulSoup
from urllib.request import urlopen, Request
import re
import requests  # ???
from pymongo import MongoClient

cluster = MongoClient("mongodb+srv://grigoriishveps:g89091259328@cluster0.6ihib.mongodb.net/test?retryWrites=true&w=majority")
db = cluster["classification_model"]
collection = db["my_texts"]


def main():
    logging.info(f"Hello ! From durable activity")

    dict_all_rows = {}

    url = 'https://experience.tripster.ru/experience/Saint_Petersburg/'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    total_pages = int(soup.find("div", class_="p-pagination").find_all("a", class_="page")[-1].text)

    i = 0
    # Формироание множества ссылок присутствующих в БД
    links_set = set()
    for elem in collection.find({'name_site': 'Tripster'}):
        links_set.add(int(re.search(r'(?<=/experience/)\d+', elem['link'])[0]))


    for page in range(1, total_pages + 1):

        url = 'https://experience.tripster.ru/experience/Saint_Petersburg/?page=' + str(page)

        response = requests.get(url)
        if response:

            soup = BeautifulSoup(response.text, 'html.parser')
            soup_optimize = soup.find('div', class_='exp-snippet-list')  # Отзывы крашили программу

            for link in soup_optimize.findAll("a", href=re.compile("(/experience/\d+/).*"), class_="title"):
                # Сказать, что питон - странный. Ниче не сказать .
                # Без класса работает 1 раз нормально
                # А второй раз он берет левые ссылки, которые не брал в первый раз
                # Как это объяснить ??!?!?!?
                if 'href' in link.attrs:
                    if int(re.search(r'(?<=/experience/)\d+', link.attrs['href'])[0]) not in links_set:

                        new_page = link.attrs['href']
                        url_page = "https://experience.tripster.ru" + new_page
                        links_set.add(int(re.search(r'(?<=/experience/)\d+', new_page)[0]))

                        content_page = BeautifulSoup(requests.get(url_page).text, 'html.parser')
                        name = content_page.find("h1", class_='top__title').text
                        content1 = content_page.find("div", {'class': 'expage-lead'})
                        try:
                            content2 = content_page.find("section", {'class': 'expage-content'}).div
                        except Exception:
                            print("Error site with name  --- ", name )
                            continue
                        text = content1.getText() + content2.getText()


                        right_box = content_page.find('div', class_='exp-info')
                        group_or_not = right_box.find('div', class_='exp-info__title').text
                        price = right_box.find('span', class_='price').text

                        all_rows = right_box.find_all('div', class_='row')
                        dict_all_rows = {}
                        for k in range(len(all_rows)):

                            data = all_rows[k].find('div', class_='label').text
                            value = all_rows[k].find('div', class_='info').text

                            if data == "Длительность":
                                data = "time"
                            elif data == "Размер группы":
                                data = "count_peoples"
                            elif data == "Дети":
                                data = "child"
                            elif data == "Как проходит":
                                data = "is_on_foot"
                                value = value == "Пешком"
                            elif data == "Рейтинг":
                                data = "rating"
                                value = value.split("по")[0] + " из 5"
                            else:
                                data = "None"

                            dict_all_rows[data] = value

                        dict_all_rows["name"] = name
                        dict_all_rows["link"] = url_page
                        dict_all_rows["group"] = group_or_not
                        dict_all_rows["price"] = price
                        dict_all_rows["text"] = text
                        dict_all_rows["name_site"] = "Tripster"

                        collection.insert_one(dict_all_rows)

                        logging.info(f"Добавлена экскурсия в Дб - {dict_all_rows['link']}")

                        i += 1

                        if i % 10 == 0:
                            # logging.info(f'Страница № {page}')
                            print(f'Страница № {page}')
                            # optim delay 60 sec
                            print(print("links ", len(links_set)))
                            time.sleep(300)

                        # if i > 10:
                        #     break
        else:
            logging.info("Ошибка с ответом от сервера")

    print("links ", len(links_set))
    for s in links_set:
        print(s)
    return f"Hello!"

if __name__ == '__main__':
    main()